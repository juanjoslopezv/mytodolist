
var TodoList = (function(){
	//Private Members
	var customDate = require("CustomDate");
	var db = require("DB");
	var TAG = "[TodoList.js]";

	function createTask(task){
		console.log(TAG, "createTask");
		db.query({
			sql : 'INSERT INTO task (text, completed, date) VALUES (?,?,?)',
			bindings : [task.text, 0, customDate.timeStamp()]
		});
	}

	function updateTask(task){
		console.log(TAG, "updateTask task = " + JSON.stringify(task));
		var date = customDate.timeStamp();
		db.query({
			sql : 'UPDATE task SET completed=?, date=?, text=? where id=?',
			bindings : [task.completed, date, task.text, task.id]
		});
	}

	function getTaskWhereTextLike(e){
		console.log(TAG, "getTaskWhereTextLike");
		var tasks = [];
		db.query({
			sql : 'SELECT id, text, completed, date FROM task where text like ? AND completed=? order by date DESC',
			bindings : ["%" + e.text + "%",e.completed],
			onSuccess : function(taskRec){
				if(taskRec ){
					console.log(TAG, "completedTask count = " + taskRec.getRowCount());
					while(taskRec.isValidRow()){
						//Initialize the object key with the id
						task = {};
						task.id = taskRec.fieldByName('id');
					  	task.text = taskRec.fieldByName('text');
					  	task.date = taskRec.fieldByName('date');
					  	task.completed = taskRec.fieldByName('completed');
					  	task.modifiedSince = customDate.timeSince(task.date);
					  	taskRec.next();
					  	tasks.push(task);
					  	//Return result via the success callback
						
					}
				}
				e.onSuccess && e.onSuccess(tasks);
			}
		});
	}

	function getCompletedTasks(e){
		console.log(TAG, "getCompletedTasks");
		var tasks = [];
		db.query({
			sql : 'SELECT id, text, completed, date FROM task where completed=1 order by date DESC',
			onSuccess : function(taskRec){
				if(taskRec ){
					console.log(TAG, "completedTask count = " + taskRec.getRowCount());
					//Return result via the success callback
					e.onSuccess && e.onSuccess(createTasksStructure(taskRec));
				}
				
			}
		});
	}

	function getPendingTasks(e){
		console.log(TAG, "getPendingTasks");
		db.query({
			sql : 'SELECT id, text, completed, date FROM task where completed=0 order by date DESC',
			onSuccess : function(taskRec){
				if(taskRec){
					console.log(TAG, "pendingTask count = " + taskRec.getRowCount());
					//Return result via the success callback
					e.onSuccess && e.onSuccess(createTasksStructure(taskRec));
				}
			}
		});
	}

	function createTasksStructure(taskRec){
		console.log(TAG, "createTaskStructure");
		var tasks = [];
		while(taskRec.isValidRow()){
			task = {};
			task.id = taskRec.fieldByName('id');
		  	task.text = taskRec.fieldByName('text');
		  	task.date = taskRec.fieldByName('date');
		  	task.completed = taskRec.fieldByName('completed');
		  	task.modifiedSince = customDate.timeSince(task.date);
		  	taskRec.next();
		  	tasks.push(task);
			
		}
		return tasks;
	}

	function deleteTaskByID(id){
		console.log(TAG, "deleteTaskByID id = " + id);
		db.query({
			sql : 'DELETE FROM task where id=?',
			bindings : [id]
		});
		deleteImageFromTask({
			file : id
		});
	}

	function newGuid(){
		console.log(TAG, "newGuid");
	    var d = new Date().getTime();
	    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	        var r = (d + Math.random()*16)%16 | 0;
	        d = Math.floor(d/16);
	        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
	    });
	    return uuid;
	};

	function saveImageToTask(e){
		console.log(TAG, "saveImageToTask");
	 	var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,e.file + ".jpg");
	 	f.write(e.data);
	}

	function deleteImageFromTask(e){
		console.log(TAG, "deleteImageFromTask");
	 	var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,e.file + ".jpg");
	 	f.deleteFile();
	}

	function getTaskImageByID(id){
		console.log(TAG, "getTaskImageByID id = " + id);
		var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,id + ".jpg");
		return f;
	}

	//Initialize
	(function(){
		console.log(TAG, "Initializing database");
		db.init();
	})();
	//Public Members
	return {
		createTask : createTask,
		getCompletedTasks : getCompletedTasks,
		getPendingTasks : getPendingTasks,
		deleteTaskByID : deleteTaskByID,
		updateTask : updateTask,
		saveImageToTask : saveImageToTask,
		getTaskImageByID : getTaskImageByID,
		getTaskWhereTextLike : getTaskWhereTextLike
	};
})();

module.exports = TodoList;