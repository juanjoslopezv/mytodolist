
var DB = (function(){
	//Private Members
	var db;
	var DB_NAME = "TodoList3";
	var TAG = "[DB.js]";

	function query(e){
		console.log(TAG, "query");
		db = Ti.Database.open(DB_NAME);
		var result = e.bindings ? db.execute(e.sql, e.bindings) : db.execute(e.sql);
		e.onSuccess && e.onSuccess(result);
		result && result.close();
		db.close();
		db = null;
	}

	function init(){
		db = Ti.Database.open(DB_NAME);
		db.execute('CREATE TABLE IF NOT EXISTS task(id INTEGER PRIMARY KEY AUTOINCREMENT, text TEXT, completed INTEGER, date TEXT);');
		OS_IOS && db.file.setRemoteBackup(false);
		db.close();
		db = null;
	}
	//Public Members
	return {
		init : init,
		query : query
	}
})();

module.exports = DB;