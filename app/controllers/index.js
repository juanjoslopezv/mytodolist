var todo = require("TodoList");
var taskField = Ti.UI.createTextField();
var currentFocus = null;
var targetImage = null;
var TAG = "[index.js]";

function createNewTask(){
	console.log(TAG, "createNewTask");
	taskField.text = "";
	var dialog = Ti.UI.createAlertDialog({
		title: 'Create Task:',
	    style: OS_IOS ? Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT : null,
	    androidView : OS_ANDROID ? taskField : null,
	    buttonNames: ['Create','Cancel']
	});
  	dialog.addEventListener('click', function onCreateTask(e){
  		//Remove Event Listener to prevent memory leaks
  		dialog.removeEventListener('click', onCreateTask);

  		if(e.index == 0){
  			console.log(TAG, "creating task");
  			var text = OS_IOS ? e.text : taskField.value;
    		console.log(TAG, 'e.text: ' + text + " index = " +  e.index);
    		console.log(TAG, "new task id = " + todo.createTask({
    			text : text
    		}));
    		loadPendingTasks();
  		}
  		
  	});
  	dialog.show();
}

function showPendingList(){
	console.log(TAG, "showPendingList");

	if($.pendingTasks.showing){
		//Prevent animation if view is already showing
		return;
	}
	loadPendingTasks();
	$.pendingTasks.animate(Ti.UI.createAnimation({
		top : "8%",
		duration : 300
	}));

	//Set a flag so we know if $.$.pendingTasks its currently being displayed
	$.pendingTasks.showing = true;
	//Clear the flag from the other $.completedTasks view
	$.completedTasks.showing = false;

	$.completedTasks.animate(Ti.UI.createAnimation({
		top : "100%",
		duration : 300
	}));
}

function showCompletedList(){
	console.log(TAG, "showCompletedList");

	if($.completedTasks.showing){
		//Prevent animation if view is already showing
		return;
	}

	$.completedTasks.animate(Ti.UI.createAnimation({
		top : "8%",
		duration : 300
	}));
	//Set a flag so we know if $.$.completedTasks its currently being displayed
	$.completedTasks.showing = true;
	//Clear the flag from the other $.pendingTasks view
	$.pendingTasks.showing = false;

	$.pendingTasks.animate(Ti.UI.createAnimation({
		top : "-100%",
		duration : 300
	}));
}

function showLists(e){
	console.log(TAG, "showLists");
	if(e.direction == "down"){
		console.log(TAG, "swipe down");

		//Reload View to update changes
		loadCompletedTasks();

		//Hide Keyboard if current focus is defined
		currentFocus && currentFocus.blur();

		//If the user swipes down on show the two lists again
		$.pendingTasks.animate(Ti.UI.createAnimation({
			top : $.pendingTasks.overViewTop,
			duration : 300
		}));
		$.completedTasks.animate(Ti.UI.createAnimation({
			top : $.completedTasks.overViewTop,
			duration : 300
		}));
		//Clear the flags of both views
		$.completedTasks.showing = false;
		$.pendingTasks.showing = false;
	}
}

function loadCompletedTasks(){
	console.log(TAG, "loadCompletedTasks");
	todo.getCompletedTasks({
		onSuccess : function(tasks){
			addTaskToView({
				tasks : tasks,
				view : $.completedList
			});
		}
	});
	
	
}

function loadPendingTasks(){
	console.log(TAG, "loadPendingTasks");
	todo.getPendingTasks({
		onSuccess : function(tasks){
			addTaskToView({
				tasks : tasks,
				view : $.pendingList
			});
		}
	});
	
}

function onEditTask(e){
	console.log(TAG, "onEditTask");
	//Check if a text field has been click
	if(e.source.id == "task" || e.source.id == "searchPending" || e.source.id == "searchCompleted"){
		console.log(TAG, "task TextField clicked");
		currentFocus = e.source;
	}

	if(e.source.id == "taskImage"){
		console.log(TAG, "TextField clicked");
		targetImage = e.source;
		if(e.source.noImage){
			showMediaOptions();
		}else{
			openFullSizeImage(e.source.image);
		}
		
	}
}

function openFullSizeImage(img){
	console.log(TAG, "openFullSizeImage");
	$.fullSizeImage.image = img;
	$.imageTaskPreview.visible = true;
}

function showMediaOptions(){
	console.log(TAG, "showMediaOptions");
	var options = Titanium.UI.createAlertDialog({
		title : 'Get Picture From:',
		buttonNames : ['Camera','Gallery','Cancel']
	});
	options.addEventListener('click', function onClick(e){
		//Remove Event Listener to prevent memory leaks
		options.removeEventListener('click', onClick);
		if(e.index == 0){
			openCamera();
		}else if(e.index == 1){
			openMediaGallery();
		}
	});
	options.show();
}

function openCamera(){
	console.log("openCamera");
	Titanium.Media.showCamera({
		success:function(e) {
			setTaskImage(e);
		},
		error:function(error) {
			// called when there's an error
			var a = Titanium.UI.createAlertDialog({title:'Camera'});
			if (error.code == Titanium.Media.NO_CAMERA) {
				a.setMessage('Please run this test on device');
			} else {
				a.setMessage('Unexpected error: ' + error.code);
			}
			a.show();
		},
		//Only photos allowed
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
	});
}

function openMediaGallery(){
	console.log("openMediaGallery");
	Titanium.Media.openPhotoGallery({
		success:function(e) {
			setTaskImage(e);
		},
		error:function(error) {
			// called when there's an error
			var a = Titanium.UI.createAlertDialog({
				title:'Photo Gallery',
				message : 'Unexpected error: ' + error.code
			});
			a.show();
		},
		//Only photos allowed
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
	});
}

function setTaskImage(e){
	console.log(TAG, "setTaskImage");
	targetImage.image = e.media;
	todo.saveImageToTask({
		file : targetImage.taskID,
		data : e.media
	})
	targetImage.noImage = false;
	if($.imageTaskPreview.visible){
		openFullSizeImage(e.media);
	}
}

function closePreview(){
	console.log(TAG, "closePreview");
	$.imageTaskPreview.visible = false;
	//Set image to null to relase memory
	$.imageTaskPreview.image = null;
}

function changeImage(e){
	console.log(TAG, "changeImage");
	showMediaOptions();
}

function addTaskToView(e){
	console.log(TAG, "addTaskToView");
	e.view.removeAllChildren();
	_.each(e.tasks, function(task){
		task.updateLists = function(){
			loadPendingTasks();
			loadCompletedTasks();
		};
		e.view.add(Alloy.createController("todoTask",task).getView());
	});
}

function startSearch(e){
	console.log(TAG, "startSearch");
	todo.getTaskWhereTextLike({
		text : e.source.value,
		completed : $.completedTasks.showing,
		onSuccess : function(tasks){
			if($.pendingTasks.showing){
				addTaskToView({
					tasks : tasks,
					view : $.pendingList
				})
			}else{
				addTaskToView({
					tasks : tasks,
					view : $.completedList
				});
			}
			
		}
	});
}

//Initialize
(function(){
	loadPendingTasks();
	loadCompletedTasks();
})();

$.myTodoView.addEventListener('androidback', function(e){
	if($.pendingTasks.showing || $.completedTasks.showing){
		//Simulate a swipe down gesture
		showLists({
			direction : "down"
		});
	}
});


$.myTodoView.open();