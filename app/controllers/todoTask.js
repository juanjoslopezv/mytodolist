var args = arguments[0] || {};
var todo = require("TodoList");
var TAG = "[todoTask.js]";

function showShareOptions(e){
	console.log(TAG, "showShareOptions");
	if(e.direction == "left"){
		$.contentRow.animate(Ti.UI.createAnimation({
			left : "-50%",
			duration : 300
		}));
	}else if(e.direction == "right"){
		$.contentRow.animate(Ti.UI.createAnimation({
			left : "0%",
			duration : 300
		}));
	}
}

function onChangeTaskStatus(e){
	console.log(TAG, "onChangeTaskStatus completed = " + $.completed.value);
	args.completed = $.completed.value ? 1 : 0;
	todo.updateTask(args);
}

function onChangeTaskText(e){
	console.log(TAG, "onChangeTaskText");
	args.text = $.task.value;
	todo.updateTask(args);
}

function deleteTask(){
	console.log(TAG, "deleteTask");
	todo.deleteTaskByID(args.id);
	args.updateLists();
}

function shareTask(){
	console.log("shareTask");
	var options = Titanium.UI.createAlertDialog({
		title : 'Share Via:',
		buttonNames : ['Email','SMS','Cancel']
	});
	options.addEventListener('click', function onClick(e){
		//Remove Event Listener to prevent memory leaks
		options.removeEventListener('click', onClick);
		if(e.index == 0){
			sendEmail();
		}else if(e.index == 1){
			sendSms();
		}
	});
	options.show();
}

function sendEmail(){
	console.log(TAG, "sendEmail");
	var emailDialog = Ti.UI.createEmailDialog()
	emailDialog.subject = "MyTodoList App - shared a task";
	emailDialog.toRecipients = [];
	emailDialog.messageBody = 'Task : ' + args.text + '';
	emailDialog.addAttachment(todo.getTaskImageByID(args.id));
	emailDialog.open();
	
	
}

function sendSms(){
	if(OS_ANDROID){
		var intent = Ti.Android.createIntent({
	        action: Ti.Android.ACTION_SEND,
	        //packageName: 'com.android.mms',
	        //type: 'vnd.android-dir/mms-sms',
	        type : "image/*"
        });
        intent.putExtra("sms_body",'MyTodoList List task shared with you, task : ' + args.text);    
        intent.putExtra("address", ""); 
        var img = todo.getTaskImageByID(args.id);
        if(img.exists()){
        	console.log(TAG, "added Attachment nativePath = " + img.getNativePath());
        	intent.putExtraUri(Titanium.Android.EXTRA_STREAM, img.getNativePath());
        }
        
        try {
            Ti.Android.currentActivity.startActivity(intent, "Send");
        } catch (ActivityNotFoundException) {
            Ti.UI.createNotification({ message : "Error" }).show();
        }
	}else{
		showSMSIOSDialog();
	}
}

function showSMSIOSDialog() {
    var module = require('com.omorandi');
    Ti.API.info("module is => " + module);

    //create the smsDialog object
    var smsDialog = module.createSMSDialog();
    //check if the feature is available on the device at hand
    if (!smsDialog.isSupported())
    {
        //falls here when executed on iOS versions < 4.0 and in the emulator
        var a = Ti.UI.createAlertDialog({title: 'warning', message: 'the required feature is not available on your device'});
        a.show();
    }
    else
    {
        //pre-populate the dialog with the info provided in the following properties
        smsDialog.recipients = [''];
        smsDialog.messageBody = 'MyTodoList List task shared with you, task : ' + args.text;

        //set the color of the title-bar
        smsDialog.barColor = 'black';

        //add attachments if supported
        if (smsDialog.canSendAttachments()) {
            
            //add an attachment as a TiBlob
            var img = todo.getTaskImageByID(args.id);
            if(img.exists()){
            	smsDialog.addAttachment(img);
            }
            
        }
        else {
            Ti.API.info('We cannot send attachments');
        }

        //add an event listener for the 'complete' event, in order to be notified about the result of the operation
        smsDialog.addEventListener('complete', function(e){
            Ti.API.info("Result: " + e.resultMessage);
            var a = Ti.UI.createAlertDialog({title: 'complete', message: 'Result: ' + e.resultMessage});
            a.show();
            if (e.result == smsDialog.SENT)
            {
                //do something
            }
            else if (e.result == smsDialog.FAILED)
            {
               //do something else
            }
            else if (e.result == smsDialog.CANCELLED)
            {
               //don't bother
            }
        });

        //open the SMS dialog window with slide-up animation
        smsDialog.open({animated: true});
    }
};

//Initialize
(function(){
	console.log(TAG ,"got task = " + JSON.stringify(args));
	$.completed.value = args.completed == 1 ? true : false;
	$.task.value = args.text;
	$.modifiedSince.text = args.modifiedSince + " ago";
	var imgBlob = todo.getTaskImageByID(args.id);
	if(imgBlob.exists()){
		console.log(TAG, "image found for task id = " + args.id);
		$.taskImage.image = imgBlob.read();
		$.taskImage.noImage = false;
	}
	$.taskImage.taskID = args.id;
})();